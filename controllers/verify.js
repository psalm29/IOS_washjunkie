washjunkie.controller('verifyController',['$scope', ($scope) => {
    window.page = () =>{
        document.querySelector('.ios .navbar ~ .page-content').style.padding = "0 0 0 0 ";
    }
    window.callFun();
    console.log('verify');
    const eraseBtn = document.getElementById('erase-btn'),
        emptyInput = document.getElementById('verify');
    const submit = document.getElementById('submit');
    let btns = document.querySelectorAll('button'),
        inputValue = document.querySelector('#verify').value;
    for (let i = 0; i < btns.length; i++) {
        btns[i].addEventListener('click', () => {
            let eneteredValue = inputValue;
            eneteredValue += btns[i].value;
            emptyInput.value = eneteredValue;
            inputValue = eneteredValue;
        });
        
    }
    eraseBtn.addEventListener('click', () => {
        let codeInput = inputValue.slice(0, -1);
        inputValue = codeInput;
        emptyInput.value = inputValue;
    });

    submit.addEventListener('click', () =>{
        let code = inputValue;
        confirmationResult.confirm(code).then((result) =>{
            // User signed in successfully
            let user = result.user;
            console.log(user);
        }).catch((error) =>{
            // User couldn't sign in (bad Verification code?)
            if(error){
                location.href = '#/login';
            }
        });
        code.value = '';

        firebase.auth().onAuthStateChanged((user) =>{
            if(user){
                app.preloader.show();
                var phone = localStorage.getItem('phone_number');
                    setTimeout(() => {
                        app.preloader.hide();
                        location.href = '#/wallet';
        
                    }, 1000);
                console.log(firebase.auth().currentUser);
                
            }
            else{
                    app.preloader.show();
                    setTimeout(()=>{
                        
                        swal('Ooops...', 'Please Enter a valid code ', 'error');
                        console.log(swal())
                        app.preloader.hide();
                        location.href = '#/verify';
                    },1000)
                }
            
            
        });

    });
  
    window.callFun();
    window.page();


}]);