
washjunkie.controller('walletController', ['$scope', function($scope) {
    window.page = () =>{
        document.querySelector('.ios .navbar ~ .page-content').style.padding = "56px 0 0 0 ";
    }

    window.callFun = ()=>{
        const more = document.getElementById('more').style.display = "block",
            camera = document.getElementById('camera').style.display ="none",
            footer = document.querySelector('#footer').style.display = "block";
            header = document.querySelector('#header').style.display = "block";
    }
    let userPhoneNumber = localStorage.getItem('phone_number');
    if (userPhoneNumber.startsWith('+234', 0)) {
        let rawNum = userPhoneNumber;
        userPhoneNumber = rawNum.slice(4, rawNum.length);
    }

    const database = firebase.database();
    const currentWalletBalance = document.getElementById('current-balance');
    const lastWalletDeposit = document.getElementById('last-deposit');
    const tableBody = document.querySelector('tbody');
    const selectOtherAddress = document.getElementById('select-address');
    const placeOrderBtn = document.getElementById('place-order-btn');

    const navbarName = document.getElementById('navbar-name');
    const navbarNumber = document.getElementById('navbar-num');

    let userDBRef = database.ref('test-users');
    console.log(userPhoneNumber);
    userDBRef.orderByChild('phone').equalTo(userPhoneNumber).on('value', snapshot => {
        // console.log(snapshot.val());
        let walletHistory = {};
        let walletHistoryArr = [];
        let userdata = snapshot.val();
        let uuid = Object.keys(userdata)[0];
        let addressOptions = [];
        for (const key in userdata) {
            if (userdata.hasOwnProperty(key)) {
                const element = userdata[key];
                navbarName.innerText = element.name;
                navbarNumber.innerText = userPhoneNumber || element.phone;
                walletHistory = element.walletHistory;
                selectOtherAddress.insertAdjacentHTML('beforeend', `<option val='${element.address}'>${element.address}</option>`);
                if (!element.walletHistory) {
                    lastWalletDeposit.innerHTML = '<span style="color: #F5818E">&#8358' + 0 + '</span>';
                }
                if (Number(element.wallet) > 1000) {
                    currentWalletBalance.innerHTML = `<span style='color: #07C6CA'>&#8358;${element.wallet}</span>`;
                } else {
                    currentWalletBalance.innerHTML = `<span>&#8358;${element.wallet}</span>`;
                }
            }
        }
        if (userdata[uuid].otherAddress) {
            for (const key in userdata[uuid].otherAddress) {
                if (userdata[uuid].otherAddress.hasOwnProperty(key)) {
                    const element = userdata[uuid].otherAddress[key];
                    addressOptions.push(element);
                }
            }
            for (let i = 0; i < addressOptions.length; i++) {
                const element = addressOptions[i];
                selectOtherAddress.insertAdjacentHTML('beforeend', `<option val='${element}'>${element}</option>`);
            }
        }
        for (const key in walletHistory) {
            if (walletHistory.hasOwnProperty(key)) {
                const element = walletHistory[key];
                walletHistoryArr.push(element);
            }
        }
        tableBody.innerHTML = '';
        for (let i = walletHistoryArr.length - 1; i >= 0; i--) {
            lastWalletDeposit.innerHTML = `<span>&#8358;${walletHistoryArr[walletHistoryArr.length - 1].amount}</span>`;
            const element = walletHistoryArr[i];
            let _date = element.date;
            let formattedDate = _date.split(',');
            let tr = `<tr><td class='label-cell'><p><span style='color: #03C5C9'>${element.method}</span><br><span style='color: black'>&#8358;${element.amount}</span></p></td><td class='numeric-cell'><p>${formattedDate[1]}<br><span>${formattedDate[0]}</span></p></td></tr>`;
            tableBody.innerHTML += tr;

        }
        // console.log(walletHistoryArr);
    });

    let unknownOrderDBRef = database.ref('unknownOrder');
    unknownOrderDBRef.orderByChild('phone').equalTo(userPhoneNumber).on('value', snapshot => {
        // console.log(snapshot.val());
        let data = snapshot.val();
        let pendingOrderArr = [];
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                const element = data[key];
                pendingOrderArr.push(element);
            }
        }
        // console.log(pendingOrderArr);
        let orderStatusDiv = document.querySelector('.pending-order ul li .item-inner');
        orderStatusDiv.innerHTML = '';
        let currentOrder = pendingOrderArr[pendingOrderArr.length - 1];
        let formattedDate = currentOrder.date.split(',');

        let appendOrder = `<div class='item-title' style="display: grid; grid-template-columns: 1fr 1fr; width: 100%;"><p>Order Number: </p><p style='text-align: right;'>${currentOrder.orderNum}</p></div><div class='item-title' style="display: grid; grid-template-columns: 1fr 1fr; width: 100%;"><p>Date: </p><p style='text-align: right;'>${formattedDate[0]}</p></div><div class='item-title' style="display: grid; grid-template-columns: 1fr 1fr; width: 100%;"><p>Time: </p><p style='text-align: right'>${formattedDate[1]}</p></div><div class='item-title' style="display: grid; grid-template-columns: 1fr 1fr; width: 100%;"><p>Status: </p><p style='text-align: right; text-transform: capitalize;'>${currentOrder.status}</p></div>`;

        orderStatusDiv.insertAdjacentHTML('beforeend', appendOrder);
    });

    function createOrderNum() {
        var num = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (var i = 0; i < 5; i++)
            num += possible.charAt(Math.floor(Math.random() * possible.length));
        return num;
    }

    placeOrderBtn.addEventListener('click', () => {
        console.log('log');
        userDBRef.orderByChild('phone').equalTo(userPhoneNumber).once('value', snapshot => {
            let data = snapshot.val();
            let name;
            let today = new Date().toLocaleString('en-NG', { timezone: 'WAT' });
            let orderNum = createOrderNum();
            let dbRef = database.ref('unknownOrder');

            for (const key in data) {
                if (data.hasOwnProperty(key)) {
                    const element = data[key];
                    name = element.name;
                }
            }
            if (!selectOtherAddress.value) {
                swal({ type: 'error', title: 'Empty input field', text: 'Order can\'t be made. Please select an address' });
            } else {
                dbRef.push({
                    phone: userPhoneNumber,
                    name,
                    orderNum,
                    date: today,
                    status: 'pending',
                    deliverTo: selectOtherAddress.value
                }).then(() => { swal('Order successfuly made. We shall reach out to you soon for confirmation') });
            }
        })
    });

    console.log('Wallet');
    window.callFun();
    window.page();
}]);