

let washjunkie = angular.module('washjunkie', ['ngRoute']);

washjunkie.config(['$routeProvider', function($routeProvider) {

    $routeProvider

        .when('/', {
           templateUrl: 'views/home.html',
            controller: 'homeController'
        })
        .when('/about', {
           templateUrl: 'views/about.html',
            controller: 'aboutController'
        })
        .when('/login', {
            templateUrl: 'views/login.html',
            controller:  'loginController'
        })
        .when('/welcome', {
            templateUrl: 'views/welcome.html',
            controller: 'welcomeController'
        })
        .when('/begin', {
            templateUrl: 'views/begin.html',
            controller:  'beginController'
        })
        .when('/tailor',{
            templateUrl: 'views/tailor.html',
            controller: 'tailorController'
        })
        .when('/wallet', {
            templateUrl: 'views/wallet.html',
            controller:  'walletController'
        })
        .when('/history',{
            templateUrl: 'views/history.html',
            controller: 'historyController'
        })
        .when('/profile',{
            templateUrl: 'views/profile.html',
            controller: 'profileController'
        })
        .when('/verify', {
            templateUrl: 'views/verify.html',
            controller: 'verifyController'
        })
        .otherwise({
        redirectTo: '/'
      });

}]);








